#!/usr/bin/env bash

# Fix minGW build
export RTLSDR_REF=d770add42e87a40e59a0185521373f516778384b
export RTL433_REF=21.12

docker buildx build \
  -t kpine/rtl433:latest \
  -t kpine/rtl433:$RTL433_REF \
  --label "version=$RTL433_REF" \
  --platform linux/amd64,linux/arm,linux/arm64 \
  --build-arg NPROC="$(($(nproc)-1))" \
  --build-arg RTLSDR_REF="$RTLSDR_REF" \
  --build-arg RTL433_REF="$RTL433_REF" \
  --no-cache \
  --push \
  .
