#!/usr/bin/env bash

# Fix minGW build
export RTLSDR_REF=d770add42e87a40e59a0185521373f516778384b
export RTL433_REF=21.12

docker buildx build \
  -t kpine/rtl433:test \
  --progress plain \
  --label "version=$RTL433_REF" \
  --platform linux/arm \
  --build-arg NPROC="$(($(nproc)-1))" \
  --build-arg RTLSDR_REF="$RTLSDR_REF" \
  --build-arg RTL433_REF="$RTL433_REF" \
  --push \
  .
