#
# Common base image for builder and app
#
FROM alpine:3.15 as base

RUN apk add --no-cache \
      libssl1.1=~1.1 \
      libusb=~1.0


#
# RTLSDR Builder image
#
FROM base as rtlsdr-builder

RUN apk add --no-cache \
      build-base=~0.5 \
      clang=~12 \
      cmake=~3.21 \
      git=~2.34 \
      libusb-dev=~1.0 \
      openssl-dev=~1.1 \
      samurai=~1.2

ARG RTLSDR_REF
ARG NPROC=1
ENV SAMUFLAGS="-j $NPROC"

WORKDIR /src/rtl-sdr
RUN git clone --no-checkout git://git.osmocom.org/rtl-sdr . \
 && git checkout $RTLSDR_REF

WORKDIR /src/rtl-sdr/build
RUN CC=/usr/bin/clang CXX=/usr/bin/clang++ \
    cmake -GNinja -DCMAKE_BUILD_TYPE=Release -DINSTALL_UDEV_RULES=OFF -DDETACH_KERNEL_DRIVER=OFF -DENABLE_ZEROCOPY=ON .. \
 && cmake --build . \
 && cmake --build . --target install \
 && rm -f /usr/local/lib/librtlsdr.so*


#
# rtl_433 Builder image
#
FROM rtlsdr-builder as builder

ARG RTL433_REF
ARG NPROC=1
ENV SAMUFLAGS="-j $NPROC"

WORKDIR /src/rtl_433
RUN git clone --no-checkout https://github.com/merbanan/rtl_433 . \
 && git checkout $RTL433_REF

WORKDIR /src/rtl_433/build
RUN CC=/usr/bin/clang CXX=/usr/bin/clang++ \
    cmake -GNinja -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH=/dist -DFORCE_COLORED_BUILD=ON -DENABLE_RTLSDR=ON -DENABLE_SOAPYSDR=OFF -DENABLE_OPENSSL=ON  .. \
 && cmake --build . \
 && cmake --build . --target install


#
# App Image
#
From base as app

ARG RTLSDR_REF
ARG RTL433_REF

LABEL description="Minimal container for running rtl_433"
LABEL rtl-sdr-reference="$RTLSDR_REF"
LABEL rtl_433-reference="$RTL433_REF"

WORKDIR /config
VOLUME /config

COPY --from=builder /dist/bin/rtl_433 /usr/local/bin/

ENTRYPOINT ["rtl_433"]
